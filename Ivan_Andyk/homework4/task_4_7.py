from collections import Counter

mystr = "Hello world! Our world is so beautiful!"
lwr_mystr = mystr.lower()
new_str = list()

for i in lwr_mystr:
    if i == '.' or i == ',' or i == ' ' or i == '!' or i == '?' or i == ':':
        continue
    else:
        new_str.append(i)
        new_str.sort()

print(Counter(new_str).most_common(1)[0][0])
