# Likes
def likes(a):
    if len(a) == 0:
        # Не знаю как тут определить язык
        print("No one like this")
    elif len(a) == 1:
        if lang == 'ru':
            print(f"{''.join(a)} нравится это!")
        else:
            print(f"{''.join(a)} likes this!")
    elif len(a) == 2:
        if lang == 'ru':
            print(f"{a[0]} и {a[1]} нравится это!")
        else:
            print(f"{a[0]} and {a[1]} like this!")
    elif len(a) == 3:
        if lang == 'ru':
            print(f"{a[0]}, {a[1]} и {a[2]} нравится это!")
        else:
            print(f"{a[0]}, {a[1]} and {a[2]} like this!")
    elif len(a) > 3:
        others = len(a) - 2
        if lang == 'ru':
            print(f"{a[0]}, {a[1]} и {others} нравится это!")
        else:
            print(f"{a[0]}, {a[1]} and {others} others like this")


s = input("Enter names using space: ")
names = s.split()
lang_ru = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
lang = 'en'
for ru in names[0]:
    if ru.lower() in lang_ru:
        lang = 'ru'
# print(names)
likes(names)

# Анна
# Анна Виктория
# Анна Виктория Максим
# Руслан Олег Александр Анна
# Ann
# Ann Alex
# Ann Alex Mark
# Ann Alex Mark Max
