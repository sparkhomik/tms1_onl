# Task 6.1 Card validation

from collections import Counter


def validate(code):
    """ This function validates card existence possibility"""
    reverse_code = code[::-1]
    list1 = []
    for i in enumerate(reverse_code, 1):
        list1.append(i)
    d = dict(list1)
    list2 = []
    for k, v in d.items():
        if k % 2 != 0:
            list2.append(int(v))
            continue
        elif k % 2 == 0:
            v = int(v) * 2
            list2.append(v)
    numbers_list = []
    for number in list2:
        if number > 9:
            sum_digits = 0
            while number > 0:
                digit = number % 10
                sum_digits = sum_digits + digit
                number = number // 10
            numbers_list.append(sum_digits)
        else:
            numbers_list.append(number)
    if sum(numbers_list) % 10 == 0:
        return True
    else:
        return False


def validate_input(code_input):
    """This function checks is entered number corresponds to requirements"""
    dict_code = Counter(list(code_input))
    for k1 in dict_code.keys():
        if not k1.isnumeric():
            return "Entered card number includes unsupported chars or spaces"
    if len(code_input) == 0:
        return "Empty card number value"
    else:
        return validate(code_input)


print(validate_input(input("Enter card number without spaces: ")))
