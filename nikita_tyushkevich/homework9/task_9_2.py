# Bank deposit
class Investment:

    def __init__(self, invest_term, invest_amount, percent):
        self.invest_term = invest_term
        self.invest_amount = invest_amount
        self.percent = percent


class Bank:

    def deposit(self, invest):
        """This method calculates client's profit from investment
        using formula: S = P * (1 + (N * d) / (100 * D)) ** n
        S - Result sum
        n - the number of interest transactions carried out to the body of
        the deposit during the full term of the agreement
        P - originally deposited amount of money
        N - annual interest term
        d - (30) amount of days, for which percents before capitalisation are
        accrued
        D - (365) days in a year"""
        invest.percent = 10
        # Процентная ставка умноженная на колчество дней (30),
        # за которые начисляются % капитализации
        perc_cap = 10 * 30
        # Рассчёт степени начисления процентов
        el_value = invest.invest_term * 12
        elevate_value = (1 + perc_cap / (100 * 365)) ** el_value
        # Рассчёт итоговой суммы
        amount_money = invest.invest_amount * elevate_value
        return f"You will get {round(amount_money, 2)} rubles"


investor = Investment(5, 100000, 10)
bank = Bank()
print(bank.deposit(investor))
