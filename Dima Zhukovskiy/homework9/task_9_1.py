class Book:
    def __init__(self, name_book, author, page, isbn, took, reserved):
        self.name_book = name_book
        self.author = author
        self.page = page
        self.isbn = isbn
        self.took = took
        self.reserved = reserved


class People:
    def __init__(self, name):
        self.name = name

    def get_book(self, book):
        if book.took is False:
            print(f'{self.name} took the book {book.name_book}')
            book.took = True
        else:
            print(f'Book {book.name_book} absent')

    def reserve_book(self, book):
        if book.reserved is False and book.took is False:
            print(f'{self.name} reserved the book {book.name_book}')
            book.reserved = True
        elif book.reserved or book.took:
            print(f'Book {book.name_book} already reserved')

    def return_book(self, book):
        if book.took is True:
            print(f'{self.name} return book {book.name_book}')
            book.took = False


book_1 = Book('Harry Potter and Philosopher stone',
              'Joanne Rowling', 400, '5-8451-0512-9', False, False)
book_2 = Book('Harry Potter 2 ', 'Joanne Rowling',
              420, '5-1568-0512-9', False, False)
peolpe_1 = People('Dima')
peolpe_2 = People('Yana')

peolpe_1.get_book(book_1)
print(book_1.took)
peolpe_2.reserve_book(book_2)
print(book_2.reserved)
peolpe_1.return_book(book_1)
print(book_1.took)
