alphabet = set('абвгдеёжзийклмнопрстуфхцчшщъыьэюя')


def likes(*names):
    # переменная определяющая язык, по дефолту англ
    lang = 0
    for i in names[0]:
        if i.lower() in alphabet:
            # если в первом элементе массива names есть русские буквы,
            # тогда мы меняем значение переменной на 1
            lang = 1
    # выводим текст в зависимости от количества имен и языка
    if len(names) == 0:
        if lang == 0:
            print("no one likes this")
        else:
            print("никто не лайкнул это")
    elif len(names) == 1:
        if lang == 0:
            print(f"{names[0]} likes this")
        else:
            print(f"{names[0]} поставил(а) лайк")
    elif len(names) == 2:
        if lang == 0:
            print(f"{names[0]} and {names[1]} like this")
        else:
            print(f"{names[0]} и {names[1]} поставили лайк")
    elif len(names) == 3:
        if lang == 0:
            print(f"{names[0]}, {names[1]} and {names[2]} like this")
        else:
            print(f"{names[0]}, {names[1]} и {names[2]} поставили лайк")
    else:
        count_users = len(names) - 2
        if lang == 0:
            print(f"{names[0]}, {names[1]} and {count_users} others like this")
        else:
            print(f"{names[0]}, {names[1]} "
                  f"и ещё {count_users} пользователя поставили лайк")


likes("Ann")
likes("Ann", "Alex")
likes("Ann", "Alex", "Mark")
likes("Ann", "Alex", "Mark", "Max")


likes("Влад")
likes("Влад", "Дима")
likes("Влад", "Дима", "Лиля")
likes("Влад", "Дима", "Лиля", "Александра")
