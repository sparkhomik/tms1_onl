for i in range(1, 101, 1):
    if i % 3 == 0 and i % 5 == 0:
        print('fuzzbuzz')
    elif i % 3 == 0:
        print('Fuzz')
    elif i % 5 == 0:
        print('Buzz')
    else:
        print(i)
