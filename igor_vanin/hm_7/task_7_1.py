list_numbers_1 = [1, 2, 3, 4, 5, 6]
lines_out_1 = 2
column_1 = 3

list_numbers_2 = [1, 2, 3, 4, 5, 6, 7, 8]
lines_out_2 = 4
column_2 = 2


def reshape(lst_numbers: list, lines: int, columns: int):
    lst_lines = [list()] * lines
    counter_column = 0
    result_list = []
    for ln in lst_lines:
        template = []
        for i in range(columns):
            cl = lst_numbers[counter_column]
            counter_column += 1
            template.append(cl)
        result_list.append(template)
    return print(result_list)


# Test
reshape(list_numbers_1, lines_out_1, column_1)
reshape(list_numbers_2, lines_out_2, column_2)
