import re

text_encode = input('Введите текст для шифрования: ')
step_encode = int(input('Число для смещения: '))
text_dec = input('Введите текст для дешифровки: ')
step_decode = int(input('Шаг для расшифровки: '))


def cipher_cesar_encode(text, step):
    alphabet_eu_validate = re.findall(r'[aA-zZ]', text)
    alphabet_ru_validate = re.findall(r'[аА-яЯ-ёЁ]', text)
    alph_eu_EU = """ABCDEFGHIJKLMNOPQRSTUVWXYZ
                    abcdefghijklmnopqrstuvwxyz"""

    alph_ru_RU = """АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ
                    абвгдеёжзийклмнопрстуфхцчшщъыьэюя"""
    result = ''
    if alphabet_eu_validate:
        for i in text:
            position = alph_eu_EU.find(i)
            change_position = position + step
            if i in alph_eu_EU:
                result += alph_eu_EU[change_position]
            else:
                result += i

    if alphabet_ru_validate:
        for i in text:
            position = alph_ru_RU.find(i)
            change_position = position + step
            if i in alph_ru_RU:
                result += alph_ru_RU[change_position]
            else:
                result += i
    return print(result)


def cipher_cesar_decode(text, step):
    alphabet_eu_validate = re.findall(r'[aA-zZ]', text)
    alphabet_ru_validate = re.findall(r'[аА-яЯ-ёЁ]', text)
    alph_eu_EU = """ABCDEFGHIJKLMNOPQRSTUVWXYZ
                    abcdefghijklmnopqrstuvwxyz"""

    alph_ru_RU = """АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ
                    абвгдеёжзийклмнопрстуфхцчшщъыьэюя"""
    result = ''

    if alphabet_eu_validate:
        for i in text:
            position = alph_eu_EU.find(i)
            change_position = position - step
            if i in alph_eu_EU:
                result += alph_eu_EU[change_position]
            else:
                result += i

    if alphabet_ru_validate:
        for i in text:
            position = alph_ru_RU.find(i)
            change_position = position - step
            if i in alph_ru_RU:
                result += alph_ru_RU[change_position]
            else:
                result += i
    return print(result)


cipher_cesar_encode(text_encode, step_encode)
cipher_cesar_decode(text_dec, step_decode)
