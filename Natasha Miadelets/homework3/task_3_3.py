a = "Ivanou Ivan"
print(a[7:11] + " " + a[:7])

# или так
commands = a.split()
c = commands[::-1]
print(' '.join(c))
