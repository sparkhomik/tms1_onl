import math


class Flower:
    # Создаем цветы
    def __init__(self, name: str, color: str, lenght: int, cost: int,
                 days_of_life: int, date: list):
        self.name = name
        self.lenght = lenght
        self.cost = cost
        self.color = color
        self.days_of_life = days_of_life
        self.date = date

    def __str__(self):
        return f"{self.name}, цвет {self.color}, длина стебля {self.lenght}," \
               f" стоимость {self.cost}, дата среза {self.date}"


class Bouquet:
    # Создаем букет
    def __init__(self, *flower):
        self.bouquet_list = list(flower)

    # Узнаем стоимость букета
    def get_bouquet_cost(self):
        cost_list = []
        for flower in self.bouquet_list:
            cost_list.append(flower.cost)
        return sum(cost_list)

    # Ищем есть ли цветок в букете
    def find_flower(self, name):
        for flower in self.bouquet_list:
            if flower == name:
                return "Цветок есть в букете"
            else:
                return "Цветка нет в букете"

    # Сортировка по цвету/стоимости/длине стебля/свежести
    def sort_flower(self):
        flower_color = []
        flower_lenght = []
        flower_cost = []
        flower_date = []
        for flower in self.bouquet_list:
            flower_color.append(flower.color)
            flower_lenght.append(flower.lenght)
            flower_cost.append(flower.cost)
            flower_date.append(flower.date)

        return f'отсортироано: по длине стебля {sorted(flower_lenght)},' \
               f'по стоимости {sorted(flower_cost)},' \
               f' по цвету в алфавитном порядке {sorted(flower_color)},' \
               f' по свежести {sorted(flower_date)}'

    # Определяем время увядания по среднему времени жизни цветов
    def withering_time(self):
        days_of_life = []
        flower_date = []
        for flower in self.bouquet_list:
            days_of_life.append(flower.days_of_life)
            flower_date.append(flower.date)
        days_of_life_bouquet = math.ceil(sum(days_of_life) / len(days_of_life))
        # День, когда букет завянет: к самому раньшесрезанному цветку
        # добавляем среднее время жизни цветов в букете
        sorted_bouquet = sorted(flower_date)
        day_withering_time = sorted_bouquet[0][2] + days_of_life_bouquet
        sorted_bouquet[0][2] = day_withering_time
        return f'Дата увядания букета {sorted_bouquet[0]}'

    # Поиск цветка по параметру и значению
    def search_flower_in_bouquet(self, parametr: str, value):
        for flower in self.bouquet_list:
            # создаю словарь со всеми и атрибутами и значениями атрибутов
            dic = {attr: getattr(flower, attr) for attr in flower.__dict__}
            for key in dic.keys():
                if key == parametr and dic[key] == value:
                    return f"Это {flower.name}"


rose_red = Flower("Роза", "красная", 40, 10, 5, [2021, 7, 13])
tulip_white = Flower("Тюльпан", "белый", 60, 12, 6, [2021, 7, 10])
poppy_pink = Flower("Мак", "розовый", 70, 15, 7, [2021, 7, 11])
chamomile_white = Flower("Ромашка", "оранжевый", 65, 5, 7, [2021, 7, 13])
lily_yellow = Flower("Лилия", "желтый", 50, 7, 7, [2021, 7, 12])
bouquet = Bouquet(rose_red, tulip_white, poppy_pink, chamomile_white)
print(bouquet.get_bouquet_cost())
print(bouquet.find_flower(tulip_white))
print(bouquet.sort_flower())
print(bouquet.withering_time())
print(bouquet.search_flower_in_bouquet(parametr="cost", value=15))
