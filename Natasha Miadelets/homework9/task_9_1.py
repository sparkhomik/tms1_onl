class Book:
    def __init__(self, name_book, author, number_page, ISBN,
                 taken, reserved):
        self.name_book = name_book
        self.author = author
        self.number_page = number_page
        self.ISBN = ISBN
        self.taken = taken
        self.reserved = reserved


class User:
    def __init__(self, name, taken_books=None):
        if taken_books is None:
            taken_books = []
        self.name = name
        self.taken_books = taken_books

    def take_book(self, book):
        if book.taken is False:
            print(f'Вы {self.name} взяли книгу {book.name_book}')
            book.taken = True
            self.taken_books.append(book.name_book)
        else:
            print(f'Книга {book.name_book} отсутствует')

    def return_book(self, book):
        if book.name_book in self.taken_books:
            self.taken_books.remove(book.name_book)
            book.taken = False
            print(f'{self.name} вернула книгу {book.name_book}')
        else:
            print(f'Вы не брали книгу {book.name_book}')

    def reserve_book(self, book):
        if book.reserved is False and book.taken is False:
            print(f'{self.name} зарезервировала книгу {book.name_book}')
            book.reserved = True
        elif book.reserved or book.taken:
            print(f'Книга {book.name_book} уже зарезервирована')


book_1 = Book("Путь джедая", "Максим Дорофеев", 368, "978-5-00146-427-3",
              False, False)
book_2 = Book("Ясно, понятно", "Максим Ильяхов", 448, "978-5-9614-3582-5",
              False, False)
book_3 = Book("Пиши, сокращай", "Максим Ильяхов", 440, "978-5-9614-6526-6",
              False, False)
name1 = User("Наташа")
name2 = User("Маша")
name3 = User("Даша")
name1.take_book(book_1)
name1.return_book(book_1)
name3.take_book(book_1)
name3.take_book(book_2)
name2.take_book(book_2)
name1.reserve_book(book_3)
name2.reserve_book(book_3)
