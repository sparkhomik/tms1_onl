sentence = "the quick brown fox jumps over the lazy dog"


def str_len(test):
    result = []
    sent_list = list(sentence.split(" "))

    # Finding and ignoring "the" words
    for i in sent_list:
        if i == "the":
            continue
        else:

            # list should contain number of letters in each word
            result.append(len(i))
        yield result
    print(result)


# Going thru all iterations to print each number
for i in str_len(sentence):
    continue
