"""
Создайте класс book: имя книги, автор, кол-во страниц, ISBN, флаг.

Создайте класс пользователь: выдача, возврат, бронирование, проверка статуса.
"""


class Book:
    """Класс для представления книг."""
    def __init__(self, title, author, pages, isbn, reserved=False,
                 reserved_by=None):
        """Устанавливает атрибуты для book."""
        self.title = title
        self.author = author
        self.pages = pages
        self.isbn = isbn
        self.reserved = reserved
        self.reserved_by = reserved_by


class User:
    """Абоненты библиотеки."""
    def __init__(self, user_name, books=None):
        """Устанавливает атрибуты для user."""
        if books is None:
            books = []
        self.user_name = user_name
        self.books = books

    def take_book(self, book):
        """Выдача книги."""
        if not book.reserved:
            book.reserved = True
            self.books.append(book.isbn)
            print(f"Вы взяли '{book.title}'.")
        else:
            print("К сожалению, книга недоступна.")

    def return_book(self, book):
        """Возврат книги."""
        if book.isbn in self.books:
            book.reserved = False
            self.books.remove(book.isbn)
        else:
            print("У вас нет такой книги.")

    def reserve_book(self, book):
        """Бронирование книги."""
        if not book.reserved:
            book.reserved_by = self.user_name
            book.reserved = True
            print("Книга забронирована.")
        elif book.isbn in self.books:
            print("Книга уже забронирована вами.")
        else:
            print(f"'{book.title}' забронирована другим пользователем.")


book1 = Book("Война и мир. Том 1", "Л. Н. Толстой", "448", "978-5-04-108738-8")
book2 = Book("Война и мир. Том 4", "Л. Н. Толстой", "416", "978-5-04-108743-2")
user1 = User("Екатерина")
user2 = User("Мария")

user1.take_book(book1)
user2.reserve_book(book1)
user2.take_book(book2)
user2.return_book(book1)
