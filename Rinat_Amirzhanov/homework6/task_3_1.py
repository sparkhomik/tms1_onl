def calc():
    operation = input("Какую операцию собираешь сделать: \n"
                      "1. Сложение\n2. Вычитание\n"
                      "3. Умножение\n4. Деление\n")
    a = int(input("Введите первое число: "))
    b = int(input("Введите второе число: "))
    if operation == "1":
        c = a + b
        print(f"Ответ: {c}")
    elif operation == "2":
        c = a - b
        print(f"Ответ: {c}")
    elif operation == "3":
        c = a * b
        print(f"Ответ: {c}")
    elif operation == "4":
        if b == 0:
            print("На ноль делить нельзя!")
        else:
            c = a / b
            print(c)


calc()
