# Работает по принципу Цезаря,
# только еще добавлены буквы и спец символы
# Во втором случае гибрид
# из английского и русского алфавитов

symbols1 = "A1B!C2D@E3F#G№H4I5J%K6L^M7N&O?" \
           "P8Q*R9S(T)U_P-V+W=X:Y/Z{"
symbols2 = "АZБYВXГWДVЕUЁTЖSЗRИQЙPКOЛNМmНLОK" \
           "ПJРIСHТGУФFХEЦDЧCШBЩAЪ1Ы2Ь3Э4Ю5Я6"
selection = input("Type 1|2 to select the cypher:   ")
cypher = input("Type random word or phrase or cypher:    ")
Key = int(input("Type random positive(cipher) "
                "or negative(decipher) number:   "))
cypher = cypher.upper()


def jumbled_text():
    result = ""
    if selection == "1" or selection == "2":
        for symbol in cypher:
            if symbol in symbols1:
                position = symbols1.find(symbol)
                another_position = position + Key
                result = result + symbols1[another_position]
            elif symbol in symbols2:
                position = symbols2.find(symbol)
                another_position = position + Key
                result = result + symbols2[another_position]
            else:
                result = result + symbol
    print(result)


jumbled_text()
