# Users = []
# Users_1 = ["Ann"]
# Users_2 = ["Ann", "Alex"]
Users_3 = ["Ann", "Alex", "Mark"]
# Users_4 = ["Ann", "Alex", "Mark", "Max"]


def likes(names):
    if len(names) == 0:
        print("No one likes this")
    elif len(names) == 1:
        print(f"{names[0]} likes this")
    elif len(names) == 2:
        print(f"{names[0]} and {names[1]} likes this")
    elif len(names) == 3:
        print(f"{names[0]}, {names[1]} and {names[2]} like this")
    elif len(names) == 4:
        print(f"{names[1]}, {names[2]} and "
              f""f"{len(names[2:])} others like this")


likes(Users_3)

# Users = []
# Users_1 = ["Анна"]
# Users_2 = ["Анна", "Алексей"]
# Users_3 = ["Анна", "Алексей", "Марк"]
Users_4 = ["Анна", "Алексей", "Марк", "Максим"]


def likes(names):
    if len(names) == 0:
        print("Пока никто не поставил лайк")
    elif len(names) == 1:
        print(f"{names[0]} поставил(a) лайк")
    elif len(names) == 2:
        print(f"{names[0]} и {names[1]} поставили лайки")
    elif len(names) == 3:
        print(f"{names[0]}, {names[1]} и {names[2]} поставили лайки")
    elif len(names) == 4:
        print(f"{names[1]}, {names[2]} и еще "
              f""f"{len(names[2:])} поставили лайки")


likes(Users_4)
