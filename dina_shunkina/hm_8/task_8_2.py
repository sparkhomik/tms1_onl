def decorator(func):
    def wrapper(*args):
        given_variable = []
        sought_variable = []
        number_names = {0: 'zero', 1: 'one', 2: 'two', 3: 'three',
                        4: 'four', 5: 'five', 6: 'six', 7: 'seven',
                        8: 'eight', 9: 'nine', 10: 'ten', 11: 'eleven',
                        12: 'twelve', 13: 'thirteen', 14: 'fourteen',
                        15: 'fifteen', 16: 'sixteen', 17: 'seventeen',
                        18: 'eighteen', 19: 'nineteen'}
        for entered_variable in args:
            for k, v in number_names.items():
                if entered_variable == k:
                    given_variable.append(v)

        given_variable.sort()
        for alphabetic_var in given_variable:
            for k, v in number_names.items():
                if alphabetic_var == v:
                    sought_variable.append(k)
        #                print("w", sought_variable)
        func(sought_variable)

    return wrapper


@decorator
def numbers(input_str):
    print(input_str)


numbers(1, 2, 7, 14, 5)
