import random

cows = 0
bulls = 0
true = True

while true:
    number_1 = input("enter the secret 4-digit number: ")
    number_2 = random.choice(["3219", "1234", "4519", "9123"])
    print(number_1)
    print(number_2)
    for i in zip(number_1, number_2):
        if i[0] == i[1]:
            bulls += 1
        elif i[0] in number_2:
            cows += 1
    else:
        if number_1 == number_2:
            print(f" You won! End. bulls: {bulls}, cows: {cows}")
            break
    print(f"Keep playing! bulls: {bulls}, cows: {cows}. ")
