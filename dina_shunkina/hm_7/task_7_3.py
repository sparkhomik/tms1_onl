"""
Шифр Цезаря:
1. Шифр работает на нескольких языках (русский, английский)
2. Шифр сохраняет исходный регистр и знаки препинания
3.Можно обойтись одной функцией для зашифровки
"""

ALPHABET_ru = ('абвгдеёжзиклмнопрстуфхчшщъыьэюя'
               'АБВГДЕЁЖЗИКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ')
ALPHABET_en = ('abcdefghijklmnopqrstuvwxyz'
               'ABCDEFGHIJKLMNOPQRSTUVWXYZ')


def encode_decode_ru(msg_ru, offset):
    """
    Возвращает сообщение зашифрованное шифром Цезаря на русском.
    msg - сообщение
    offset - смещение в алфавите
    """
    encrypted_alphabet_ru = ALPHABET_ru[offset:] + ALPHABET_ru[:offset]
    encrypted_ru = []
    for char in msg_ru:
        index_ru = get_char_index_ru(char, ALPHABET_ru)
        cd_char_ru = encrypted_alphabet_ru[index_ru] if index_ru >= 0 else char
        encrypted_ru.append(cd_char_ru)
    return ''.join(encrypted_ru)


def get_char_index_ru(char, alphabet_ru):
    """
    Возвращает индекс первого вхождения символа с сторку русского алфавита.
    char - символ
    """
    char_index_ru = alphabet_ru.find(char)
    return char_index_ru


def encode_decode_en(msg_en, offset):
    """
    Возвращает сообщение зашифрованное шифром Цезаря на английском.
    """
    encrypted_alphabet_en = ALPHABET_en[offset:] + ALPHABET_en[:offset]
    encrypted_en = []
    for char in msg_en:
        index_en = get_char_index_en(char, ALPHABET_en)
        cd_char_en = encrypted_alphabet_en[index_en] if index_en >= 0 else char
        encrypted_en.append(cd_char_en)
    return ''.join(encrypted_en)


def get_char_index_en(char, alphabet_en):
    """
    Возвращает индекс первого вхождения символа с сторку английского алфавита.
    """
    char_index_en = alphabet_en.find(char)
    return char_index_en


if __name__ == '__main__':
    message_ru = 'Привет! Мир'
    message_en = 'Hello! World'
    shift = 3  # Смещение алфавита
    encrypted_message_ru = encode_decode_ru(message_ru, shift)
    print('Сообщение: %s' % message_ru)
    print('Зашифрованное сообщение: %s' % encrypted_message_ru)
    encrypted_message_en = encode_decode_en(message_en, shift)
    print('Сообщение: %s' % message_en)
    print('Зашифрованное сообщение: %s' % encrypted_message_en)
